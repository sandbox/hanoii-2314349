<?php

/**
 * Implements hook_menu().
 */
function insecure_forms_menu() {
  $items['admin/config/development/performance/default'] = array(
    'title' => 'Performance',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'file path' => drupal_get_path('module', 'system'),
    'weight' => -10,
  );

  $items['admin/config/development/performance/insecure_forms'] = array(
    'title' => 'Insecure forms',
    'description' => 'Allows to specify form IDs that will be made insecure.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('insecure_forms_settings'),
    'type' => MENU_LOCAL_TASK,
    'access arguments' => array('administer site configuration'),
  );

  return $items;
}

/**
 * FAPI menu callback for module settings.
 */
function insecure_forms_settings() {
  $form['insecureforms_formids'] = array(
    '#type' => 'textarea',
    '#title' => t('Form IDs'),
    '#default_value' => variable_get('insecureforms_formids', ''),
    '#cols' => 40,
    '#rows' => 5,
    '#description' => t("Enter one form ID per line. The '*' character is a wildcard."),
  );

  return system_settings_form($form);
}

/**
 * Implements hook_form_alter().
 */
function insecure_forms_form_alter(&$form, &$form_state, $form_id) {
  if (drupal_match_path($form_id, variable_get('insecureforms_formids'))) {
    // Remove form token so that the page can be safely cached.
    // TODO: Really investigate if this poses a real security threat:
    // http://pixeljets.com/blog/csrf-avoid-security-holes-your-drupal-forms
    // And all over the web
    unset($form['#token']);
    unset($form['form_token']);
    $form['#insecure_forms'] = TRUE;
  }
}

/**
 * Implements hook_cacheobject_load().
 */
function insecure_forms_cacheobject_load($objects, $cids, $bin) {
  global $user;

  if ($bin == 'cache_form' && $user->uid) {
    foreach ($objects as $object) {
      $user_roles = array_keys($user->roles);
      // force cache_token to regular auth users so that any user retrieve
      // the same form. This brings back a security issue
      if (count($user_roles) == 1 && isset($object->data['#insecure_forms'])) {
        $object->data['#cache_token'] = drupal_get_token();
      }
    }
  }
}
